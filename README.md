# tetris-game

A simple tetris game using Javascript I made from scratch.

## Controls
`A` - moves block to the left \
`D` - move block to the right \
`S` - moves block downwards \
`O` - rotates block counterclockwise \
`P` - rotates block clockwise

[Watch YouTube Stream](https://youtu.be/YMjT7ZJ7TzE)

Demo: https://tetris-game.projects.juvarabrera.com/

